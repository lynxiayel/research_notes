# Conference Notes for [CNCC 2018](http://cncc.ccf.org.cn/)

# Keynotes (Oct.25, morning)

## [Robert E. Kahn](https://www.internethalloffame.org/inductees/robert-kahn) (2004 Turing Award, co-inventor of TCP/IP)

* What this talk is about

    * Evolving the Internet to facilitate the management of digital information

        * over both short and very long time frames
        * with simplicity of design
        * open architecture
        * independence from underlying technology

    * Focus on the Digital Object Architecture

        * Its goals
        * the basic components &protocols
        * implementations, reference software and applications

* What is the internet

    * original FNC definition still applies

        * Internet refers to the global information system that - (i) is logically linked together by a globally unique address space based on the Internet Protocol (IP) or its subsequent extensions/follow-ons; (ii) is able to support communications using the Transmission Control Protocol/Internet Protocol (TCP/IP) suite or its subsequent extensions/follow-ons, and/or other IP-compatible protocols; and (iii) provides, uses or makes accessible, either publicly or privately, high level services layered on the communications and related infrastructure described herein

    * Overall Architecture is still intact despite increases in the underlying
        technology by factors about 10 Million (computation, communication and
        storage)

* Fast forward 100 years

    * looking for information from the distant past
    * doesn't help to know if the information is on a file or a machine or the internet
    * library have dealt with this issue for a long time -- retaining physical book
        collections, and using card catalogs to then locate individual books within
        the libraries.
    * publishers in 1990s, had difficulty with the advent of digital journals and the
        need for persistent citations

* Rest of this talk is about the DOA (Digital Object Architecture)

    * DOA, as a logical extension of the internet -- with same properties of
        simplicity ,scalability, open architecture and non-proprietary
    * lower the barriers to managing the digital info in the internet, long&short
        term
    * evolves the internet and is fully compatible with it -- not intended to
        replace it
    * assumes many kinds of info systems will exist over time (some old, some new,
        some changing dynamically)
    * enables interoperability of such heterogeneous info systems with integrated
        security based on Public Key Infrastructure (PKI)

* What is a digital object

    * A DO is a sequence of bits, or a set of such sequences, incorporating a work or
        portion of a work, or other info in which a party has rights or interests,
        or in which there is value.
    * each DO has, as an essential element, an associated unique persistent
         identifier,
    * Each DO consists of one or more elements known as type-value pairs, at least
        one of which must be its ID.
    * DO Interface Protocol (DOIP) enables client software to interact directly
        with a given DO
    * Enables different info systems that support the protocol to be interoperable
        relying on use of identifiers to communicate.

* Components & Protocols of the DOA

    * Three basic components

        * DO Repositories that store DOs and from which they can be accessed via their identifiers
        * DO Registries that store metadata about individual DOs and which can be used
            to search
        * An ID/resolution system that maps given IDs to info helpful in accessing the
            DOs they identify -- known as state info

    * Two basic protocols

        * DOIP, Digital Object Identifier Protocol
        * Identifier/Resolution Protocol

* Identifiers

    * A Digital Object Identifier has the form of prefix/suffix
    * The prefix is typically a small number- such as a dotted number derived from
        it such as 86.1234 or 86.1234.abc
    * The use of dotted prefixed enables an organization to authorize multiple
        parties, divisions, or departments to create and otherwise manage the
        identifiers they create.
    * A registry of allotted prefixes (not the identifiers or identifier records
        that use them) provides information about which local identifier service to
        ask for the identifier record
    * CNRI administered the identifiers/resolution system from 1003-2015 and
        called it "Handle System". The registry of prefixes is still called the
        "Global Handle Registry", but is now administered by a non-profit Foundation
        in Geneva.

* Why the DO Architecture

    * Provide a uniform conceptual approach to managing info in digital form and to
        interact directly with that DO
    * Enables DOs & other clients to find DOs from metadata, and to access them by
        use of identifiers
    * Enables interoperability between DOs using the DOIP
    * Security is integrated in the overall architecture, enabled by the DOIP, but
        made available by the computational environment in which it is embedded,
        and which most likely executes the protocol.
    * "Datatypes" & Datatype registries are critical to understanding a O; and for
        large data sets, more generally.

* DOA eg in IOT

    * the 1st computer network (ARPANET)

* DOIP

    * each entity is a DO
    * each action is also a DO

* Reference software

    * made available by CNRI, called [Cordra](https://www.cordra.org/)

* In conclusion (or getting started)

    * Logical extension of the Internet
    * General framework agreed in ITU.T recommendation x.1255
    * Specification maintained by DONA
    * Reference software implementations available from DONA, CNRI & others
    * Infrastructure support for identifier prefixes via a decentralized set of
        MPAs
    * Local identifier services, repositories, registries run by many
        individual organizations around the world
    * All interoperable with security, and scalable
    * Community groups and/or industries actively engaged in a variety of ways


## 发展数字经济值得深思的几个问题, 李国杰(工程院院士)

* TPF, Total Productivity Factor
* 昆虫纲悖论: 通用性和个性化的矛盾

## More Science, More data... Moore's Law? [Katherine Yelick (UC Berkeley)](https://people.eecs.berkeley.edu/~yelick/)

* Data growth is outpacing computing growth

    * 700GB/s "superhuman" sensors
    * Three ingredients of machine learning: data, algorithms, machines
    * cosmoflow on tensorflow
    * ML in climate data: classification, classification+localization, object
        detection:instance segmentation
    * Material design with computation

* The challenge of ML in Science

    * correlation is not causation: per capita cheese consumption vs. people died
        from entanglement of their bed sheets
    * filtering, de-noise and curating data

    * computer science challenge: #1 continue to grow computing performance

        * traditional scaling is coming to an end
        * specialization: end game for Moore's law
        * architectures for data vs. simulation: separate jobs, compute intensive,
            nearest neighbor, all-to-all, random access

    * computer science challenge: #2 programming for specialized architectures

        * programming for diverse(specialized) architectures

            * two "unsolved" compiler problems : dependence analysis and accurate
                performance models(_autotuning avoids this problem_)
            * autotuners are code generators plus search
            * eg: realtime MRI challenge


    * computer science challenge: #3 algorithms that avoid data movement

        * data movement is expensive (energy-wise)
        * make copies of C matrix (even matrix multiply had room for improvement)

# 认知图谱与推理, host: [周明(MSRA)](https://www.microsoft.com/en-us/research/people/mingzhou/)，[唐杰(THU)](http://keg.cs.tsinghua.edu.cn/jietang/), (Oct. 25, afternoon)

* Data->knowledge->cognition
* knowledge graph: to organize knowledge, allow computer to process data
    automatially
* cognition graph: to generate knowledge, allow computer to process data
    autonomously

## 学术图谱中的认知问题, [王冠三 (MS Reseasrch)](https://www.microsoft.com/en-us/research/people/kuansanw/)

* Machine cognition of academic knowledge

* cognition: the mental process of acquiring knowledge through thoughts
    experience and reasoning: 5 aspects, attention ,sensing, endurance,
    computation,
* how to harness machine for cognition
* egs: dota2, long-term reduction of type 1 diabetes,
* NLP in a nutshell

    * y=argmaxP(y|x)
    * distributional similarity
    * discrete tokens to continuous vectors using deep learning

* NLP,  y=argmaxP(y|x)
    * x paragraphs
    * y
    * for knowledge extraction

* Distributional Similarity
    * named entities (noun phrase)
    * entity relation (verb phrase): ALbert Einstain **is the father of** the theory of relativity

* word2vec
* from word to knowledge graph embedding

    * similar word contexts->similar vicinity on the graph
    * WSDM'18, [Network Embedding as Matrix Factorization: Unifying DeepWalk, LINE, PTE, and node2vec](https://arxiv.org/abs/1710.02971)
    * CS conferences clustering using embedding, relation embedding is trikier
        **but promising**

* 204M MAG publications (25M medicine, 16M biology)

    * reasoning about diabetes in MAG: three categories: cause, symptoms, treatments
    * similar causes but no publications about treatments.

## Extremely large scale cognitive graph representation in practice, [Hongxia Yang (Alibaba)](https://sites.google.com/site/hystatistics/home)

* 持续嵌入学习Continual Embedding Learning
![](slide_snap/embedding_learning.jpg "Continual Embedding Learning")
* Graph Embedding Representative Works
![](slide_snap/embedding_learning_representative_works.jpg "Graph Embedding Representative Works")
* Data variety, maps(高德), logistics(菜鸟), ecommerce(taobao), entertainment(youku), search, ads, video
* graph model
* new generation of recommendation system based on graph embedding
* 自学习认知图谱
* [NELL, CMU](http://rtw.ml.cmu.edu/rtw/)
* graph transfer learning
* 国际化，大文娱，新零售, strategical goals of Ali

## Representation Learning, [杨洋 (ZJU)](http://yangy.org/)

* anomaly detection
* representation learning

    * learning representations of network vertexes in vector space
    *  with structural and semantic properties preserved: neighborhood, distance, age
        ,gender
    * representation vectors can be used as feature vectors
    * macro level properties of networks: dynamics(user interaction, info
        diffusion, unstable user relations), scale-free(heavy-tailed
        vertex degree)
    * [Representation Learning for Scale-free Networks, AAAI'18](https://arxiv.org/abs/1711.10755)
    * [Dynamic Network Embedding by Modeling Triadic Closure Process, AAAI'18](http://yangy.org/works/dynamictriad/dynamic_triad.pdf)
        * academic, mobile, loan, server networks

## 大规模知识图谱计算引擎, MaxGraph: Systems Support for large scale knowledge graphs, 钱正平 (Alibaba)

* Heterogeneous datasets: items, brands, payments, places, movies, users, user
    centered
* Community-based recommendation (similar users buy similar products)
* graph patterns for fraudulent activities
* common patterns
* graph exploration/visualization
* [Real-time Constrained Cycle Detection in Large Dynamic Graphs](http://www.vldb.org/pvldb/vol11/p1876-qiu.pdf)

## 基于知识图谱的语义理解, [赵东岩 (PKU)](http://www.icst.pku.edu.cn/index.php?s=/Home/Team/detail/id/33/lang/en.html)

* 高考Robot

    * 构建基础语义资源库及深度语义分析技术平台
    * 研制大规模知识库构建技术，构建学科知识库
    * 提出语义与知识表示方法，研制深度语义理解技术
    * 实现面向初等教育问题求解的知识推理

* 结构化查询语句SparQL
* 知识图谱构建
* 语义理解与问题求解
* 地理学科知识库及问答求解
* future

    * 语义搜索：智能搜索、智库系统、情报分析
    * 问答技术
    * 对话系统：领域自适应、检索式、生成式、多轮及主动对话技术

* 智能证券投资助手

## Knowledge Extraction from Software Artifacts, [黎铭 (NJU)](https://cs.nju.edu.cn/lim/)

* Major challenge: to improve the software productivity, quality and user experience
* Promising solution: Software Mining, extract the knowledge from software artifacts

    * artifacts: source code, documentation, runtime logs, specification, etc.
    * source code; lexical relationship, functional behavior

* Lexical relationship

    * programming patterns: defined by programming language
    * provide instructions to the developers for reference, identify potential
        bugs
    * frequent-pattern mining, PR-Miner. (what if both correct and defective
        usage are not frequent)
    * search for API usage over internet
    * look into the comments
    * disadvantages: too shallow

* functional behaviors

    * [On the naturalness of software](https://dl.acm.org/citation.cfm?id=2337322)
    * mining functional behavior for bug localization by exploiting program
        structure
    * identifying defective source codes using bug report

        * treating source codes as natural language, VSM, rVSM
        * modeling the relatedness by learning better representation
        * natural lang vs. programming lang: word vs. statement; flat vs.
            structural
        * general idea: learn a unified feature representation between the
            bug-report/source-code pairs.
        * new model for extract info from programming lang

    * mining functional behavior for detecting software clone

        * challenge 1: howto exploit program structures to enrich the lexical information
        * challenge 2: howto learn from the functionality similar clones

# Keynotes (Oct.26, morning)

## 黄铭钧(NUS)

* 什么是区块链
* 如何运作

    1. 用户发起交易请求
    2.

* technology

    * 数据结构：交易按记录顺序存放于只可追加的分布式帐本种
    * 区块通过加密的
    * 共识

* Consensus

    * 节点识别

        * 公链: permissionless/public，节点无绑定的ID或公私钥认证
        * 私链：permissioned/private, 节点及发出的消息需经认证

    * 共识协议(Consensus protocols.models)

        * 博彩式协议：适合公链，算力挖矿，支持海量节点
        * 拜占庭容错式协议（Byzantine Fault
        Tolerant）:适合私链，分布式众数决议，高吞吐

    * 性质
        * 安全性(safety)
        * 活性(liveness)

* 四个关键概念

    * distributedshared ledger
        * 分布式业务网络上
        * 共享日志数据
    * cryptography：交易是安全的、可靠的、可验证的、同时保证足够的透明度
    * consensus: 所有区块链节点就已验证的交易记录达成一致性共识
    * smart
        contracts:业务条款嵌入到交易数据库中，并借助链上交易为媒介来执行、生效

* 区块链的价值

    * 降低运维成本
    * cryptography
    * consensus
    * smart contracts

* 区块链技术距离成熟还有多远,性能，可扩展性，必要性

    * 去中心化(decentralization)

        * 去中心化应用(dApp): 应用后台代码运行于去中心化的P2P网络(即区块链)

    * 企业级区块链

        * high throughput

            * BitCoin 6tran/s, ETH 10tran/s,
            * hyperledger, 600-1000 tran/s
            * some claims 30,000 tran/s but not certified by 3rd party

        * ease of use

            * 接口
            * 钱包管理：不是每个人都愿意随身带着“实体钱包”或U盾

        * security and privacy

            * 强大的数据管理与溯源
            * 数据不可变性-> 交易审查
            * 访问控制-不仅仅是把钱从一个账户挪到另一个账户

        * on-chain scalability

    * scalability

        * NSU and ZJU developed BlockBench, 1st standard test suite for private
        blockchain performance test.
        * ETH: 过低的吞吐率6-10tran/s
        * HyperLodger: 受限的部署规模(最多数十个节点)
        * 亟需解决：在大规模部署中实现高吞吐率
        * 现有扩展方案

            * 利用可信日志来消除歧义:equivocation
            * 拜占庭容错式协议BFT
            * 分片Sharding
            * 博彩式协议：工作量证明(PoW),股权证明(PoS)
            * 利用可信随机过程来替代工作量证明
            * 可信日志(trusted log)和可信随机过程(trusted
            randomness)都可借助可信硬件(如IntelSGX)来实现

        * Intel SGX

            * "飞地"执行(Enclave Execution)

                * 在硬件保护的可信执行环境(即飞地，enclave)中运行非特权进程(non-privilege
                process)
                *
                在"飞地"中执行的代码对外以黑盒形式可见：别的进程(甚至操作系统进程)都禁止访问"飞地"内存或干预"飞地"内存中的进程执行
            * 远程认证(Remote attestation)

                * "飞地"认证向验证者(Validator)证明指定的代码已被可信的执行
            * 无偏随机性(Unbiased randomness):由硬件辅助生成的无偏随机数
            * 可信时间戳(Trusted elapsed time):
            依据参考时间点进行消逝时间的测量(秒级)

        * 面向可扩展性的设计原则

            * "以小博大"(Scale up by scaling
            down)--通过局部可信性增强系统总体扩容能力
            * "轻重缓急"(Prioritize consensus
            messages)--侧重共识消息的高优先级处理

    * ABCD: AI+Blockchain+Cloud+BigData

* 区块链奥运，更高、更快、更强

    * 区块链可扩展性

        * 现有的扩展方案
        * 提升可扩展性

    * 智能合约smart contracts

        * 智能合约

            * 运行在区块链上的代码+存储在区块链上的数据
            * 由所有节点共同执行

        * 优点: 可信计算(假设区块链是安全的)
        * 问题

            * 执行代价高(由于数据副本)

                *以太坊引入了gas作为每笔交易的费用，以避免计算资源被滥用

            * 执行效率低
            * 安全隐患: DAO攻击，Parity漏洞...

    * 智能合约+

        * 声明式查询declarative queries

            * 目前区块链的智能合约支持并不友好(至少在用户层面)
            * 目标：用于智能合约交互的类SQL语言
            * 优势：

                * 将查询语言与系统实现解耦
                * 易用
                * 易于表达逻辑上的约束条款

        * 更快速的运行引擎 efficient processing

            * 现有的运行引擎只支持智能合约的串行执行(sequential)
            * 目标：增加并行度(Parallelisation), 硬/软件层面
            * 关键点(难点):确保一致性

        * 安全性

            * 目前智能合约语言缺乏正式规范以及相关的安全分析
            * 目标：为智能合约的执行提供安全性保障
            * 方式：

                * 语言层面：通过静态分析去排查智能合约潜在的漏洞
                * 语言层面：设计具备“安全意识”的智能合约语言
                * 运行层面：检查违规的参考监视器

        * 关键点(难点)

            * 设计针对智能合约语言及其执行的形式化规范模型
            * 安全性vs性能牺牲

    * 区块链数据存储

        * 更强的存储支持
        * Forkbase
        ![](slide_snap/forkbase.jpg)

* 区块链应用

    * 医疗数据管理

        * 电子健康档案(EHR, Electronic Health Records)
        ![](slide_snap/ehr.jpg)
        * 美迪乐

* Conclusions

    * 区块链

        * 以去中心化的系统实现提供“中心化”数据存储服务
        * 更为友好地支持数据监管与溯源
        * 增强数据透明性，一次构建互不信任节点间的信任机制

    * “坑”

        * 大量处于投机主义的ICO活动以及过度吹捧的区块链系统实现，使得区块链技术容易沦为盲从者的“毒药”
        * 可扩展性，是永远需要放在最高优先级去考量
        * 区块链不是仅仅用来造币的！结合行业的去中心化应用(dApp)才是正道

    * 解决方案

        * 提升性能：共识模型(Consensus),运行引擎(execution),存储系统(storage)
        * 增强功能：智能合约语言，约束检查(constraint checking)
        * 友好交互：声明式编程语言(declarative language)

## 区块链和超级智能时代，吴军, Amino Capital

* 超级智能时代

    * 第一代(1956-1970) 模仿人
    * 第二代(1972-2000) 数据驱动
    * 第三代(2001-2016) 大数据+深度学习
    * 第四代(2016-now) 超级智能

* 什么是超级智能？

    * AI+IoT+BlockChain=超级智能

* 未来的世界:强连接

    * 我们需要5G么？(yes, because of IoT)

* 跟踪的好处

    * 食品、药品安全
    * 交易的安全：反欺诈
    * 健康的跟踪

        * 喷气式飞机的发动机：1000+传感器
        * 可是我们身体里没有

* 跟踪的风险和危害

    * 没有人愿意被IT巨头控制

        * 数据安全：我们能相信BAT么？
        * 价格歧视：每一个人都是受害者

    * 黑客犯罪的成本接近零，受益巨大

        * 美国每年交付几亿美元赎回医疗档案
        * IoT的安全问题可能更大

* 区块链的优点

    * 数学上很漂亮

        * 非对称的加密，公开密钥
        * 你可以确认真伪，但不需要拥有
        * 你可以访问信息，但是无法修改

    * 安全：从理论上讲无法破解
    * 方便性：随意合并打包(Merge)和拆解(Split)
    * 低成本
    * 透明

## AI MegaTrends，[凌晓峰(UWO)](https://cling.csd.uwo.ca/)

* AI is to automate/enhance Human Intelligence

    * WISC IQ Test
    * A brief history of humankind

        * 3 revolutions: cognitive, agricultural, scientific
        * 2 driving forces: aggression, intelligence (Xiaofeng Ling's theory)

* Ling's Prediction of Future AI

    * Intelligent brain:
    * Intelligent brain+: HQ, home,office,city, industry, AoE (AI of Everything)
    * Intelligent Robots: home service, senior care
    * Driverless cars

* 4 Cs

    1. Critical/analytical thinking; python/AI
    2. Creative/divergent thinking
    3. Communication
    4. Collaboration

## 人工智能推动数字经济新突破，刘通(nVidia)

* 计算力的突破

    * beyond Moore's Law
    * CUDA since 2006, parallel computing in image processing to generalized
        computing
    * GPU计算的迅速普及
    * 高性能计算: 双精度->混合精度(双精度+半精度)
    * TURING Tensor Core, 65TFLOPS FP16, 130TeraOPS INT8

* 数据科学面临的巨大难题

    * slow training times for data scientists
    * RAPIDS, 加速数据科学

        * CUDF: similar to Pandas
        * CUML
        * CUGRAPH

    * NVIDIA DGX-2: largest GPU ever created

## 低时延数据中心操作系统，[陈海波(SJTU)](https://ipads.se.sjtu.edu.cn/haibo_chen)

* 时延=金钱
* 应用需求：高吞吐到低时延

    * AMazon：100ms $1B lost
    * Google：500ms $3.5B lost

* 数据中心：低时延的关键
* 操作系统:单机到数据中心
* 挑战1：低效抽象
    * DataCenter走向ns级延迟
    *
* 挑战2：协作失衡

    * 典型Bing查询/Facebook请求涉及到数千个数据中心节点
    * “木桶效应”: 单节点突发高时延以较高概率增加整体时延

* 计算范式演化：计算与数据协同

    * 关键因素：开销
    * 吞吐优先、数据规模大、局部性强:大数据时代
    * 时延优先、高并发任务、随机性强:低时延计算?
    * 迁移计算与迁移数据的平衡

* 原位计算：面向低时延数据中心的计算范式

## 量子计算，孙晓明(中科院)

# Mobile computing and Network big data (Oct.26 afternoon)

##  An Online Learning Approach to Network Application Optimization with Guarantee, [吕自成(CUHK)](http://www.cse.cuhk.edu.hk/~cslui/)

* Multi-armed bandit problems

    * Arms
    * Actions
    * Goal
    * Regret, oracle and bandit algorithm

        * oracle: knows all and always chooses the best arm
        * bandit algorithm: selects the arms based on some criterions
        * regret: loss of reward compared with the Oracle

    * Main theme in Bandit Problems: tradeoff between exploration and
        exploitation

        * explore the less selected arms

            * select the arms that are not sampled enough for accurate
            estimation of the rewards of those arms
            * less rewarded in short-term

        * exploit the arms that have been selected before

            * select the arm with the hightes average reward
            * but may fail to identify the best arm if the estimation is
            inaccurate

    * Constrained Bandit Problem (our modified MAB from the simplest version)
    ![](slide_snap/CBP1.jpg)
    ![](slide_snap/CBP2.jpg)
    ![](slide_snap/CBP3.jpg)
    ![](slide_snap/CBP4.jpg)

##  Privacy-Preserving Service Matching for Large-scsale Data in Public Cloud Platforms, [贾小华(CityU HK)](http://www.cs.cityu.edu.hk/~jia/)

* Outline

    * pMatch: privacy-preserving task matching in crowdsourcing system
    * pRide: privacy-preserving ride hailing in Online Ride Hailing Systems
    * psRide：extension of pRide with riders sharing a taxi
    * conclusion

* search over Encrypted Data

    * searchable encryption(SE)
    * owner stores encrypted data and indexes on server
    * user sends a trapdoor of key
    * example
    * major techniques for SE

        * Searchable symmetric encryption (SSE)
        * public-key encryption with keyword search (PEKS)

* Task matching in Crowdsourcing
![](slide_snap/task_matching_in_crowdsourcing.jpg "Task Matching in Croudsourcing")

    * Difficulties for pMatch

        * Multi-requester/Multi-worker(M/M)

            * Scalability: large number of task-requesters and workers
            * Dynamics of task-requesters and workers

                * Dynamic task publications and workers revocation

            * No owner-enforced search authorization

    * Proxy Re-encryption Scheme for pMatch

        * it is keyword-based matching between task requirement and worker
        interests
        * KMS generates a pair of keys for each user, one for the user and the
        other for the broker
        * The broker re-encrypts

    * steps of pMatch

        1. registration of users

            * both task-requesters and workers are users

        2. worker submits interests (encrypted keywords)

            * Broker builds indexes of interests-workers

        3. task requester publishes task

            * Broker generates trapoor from task requirement

        4. Broker performs task-workers matching and recommends tasks to
           matched workers
        5. Worker decrypts content of matched tasks

* pRide: Privacy-preserving Ride-Hailing

    * overview: Online Ride-matching-> approximate distance computation-> secure NN computation -> privacy-preserving ride-matching
    * sketches for road network
        ![](slide_snap/sketch_for_road_network.jpg "Sketches for Road Network")

        * embed the road network into a high-dimensional space using Road
        Network Embedding technique
        * G(V,E): road network. V, junction, E road-segments
        * select reference sets R={V_{i,j}}_{1\le i\le\alpha,1\le j\le\beta}, where V_{i,j} is a random subset of
        V with 2^i nodes.
        * For v\in V, compute a sketch S(v): S(v)=(S_{V_{i,1}}(v),...,S_{V_{i,j}}(v),...,S_{V_{\alpha,\beta}}(v)), where S_{V_{i,j}}(v),=min_{w\in V_{i,j}}dist(v,w)

    * sketches for mobile user u
        ![](slide_snap/sketch_for_mobile_user.jpg "Sketches for Mobile User")
        * garbled circuit, compare encrypted values

## 无源传感器网络的相关理论与技术，[高宏(HIT)](http://software.hit.edu.cn/teacher/show/117.aspx)

* 研究背景与相关概念

    * 感知节点的供电问题
    * Battery-free sensor network

        * 不配备/不依赖电池
        * 从环境获取能量
        * 获能低，储能弱

    * 无源传感器网络特点

        * 能量波动：高低变化、分布不均
        * 网络弱连：通讯受限、时断时续
        * 计算间断：射频中断、占空比低

* 面临挑战与研究问题

    * 挑战：时间不持续、获能不稳定、分布不均衡
    * 科学问题:
        充分利用多种能量源、通过多能量源融合，实现网络能量的时空全覆盖
    * 挑战：断续频繁、通信受限
    * 科学问题：以能量为中心、建立数据流与能量分布动态匹配
    * 挑战：计算任务频繁中断、节点持续工作时间有限
    * 科学问题：以"能尽其用"为目标，以主动离散化为手段, 解决计算碎片化问题

* 哈工大一些研究结果

# 数据开拓商业新生态——零售科技创新, (Oct.26 afternoon)

* 信息孤岛，行业内、行业间数据共享

# Keynotes (Oct.27, morning)

## 计算为王，黄畅([地平线](http://www.horizon-robotics.com/))

* 边缘计算
* 训练vs推理
* 感知&建模vs预测&决策
* 数据vs计算

## 智能芯片发展的思考， [陈云霁(中科院计算所)](http://novel.ict.ac.cn/ychen/index_cn.html)

* 智能芯片是整个智能产业的基石
* 支持百万级的开发者:智能指令集设计

    * 深度学习编程语言
    * 高性能函数库设计
    * 智能编程工具链构建

* 贯通式的智能计算机课程体系

## 高通量数据流众核处理器， [范东睿(中科院计算所)](http://people.ucas.edu.cn/~fandongrui)

* DPU
* 访问带宽是计算性cpu的主要瓶颈

# 智能增长：大数据驱动的业务升级 (Oct.27, afternoon)

## 大数据驱动人力资源，[常兴龙(薪人薪事)](https://www.xinrenxinshi.com/)

* 大数据驱动HR SaaS发展

    * 人岗匹配

        * 人精确识别另一个人难度系数很大

            * 评价者认知偏差
            * 候选人表现偏差
            * 面试题偶然偏差
            * 综合性偏差

        * 人能在职场中准确自我认知的难度更大
        ![](slide_snap/zhichangziworenzhi.jpg "职场中的自我认知难度大")

    * 数据化驱动客户增长

        * CEO首签作用大，续签作用差
        * 吸粉 ->
        * 话题Topic(eg.金三银四, HR话题每周变，系统对话题三周更新) ->
        * 群体运营(语义识别的场景化处理) ->
        * 成单转化

    * 有效feature

        * 离职率预测：46%？->93%
        * 3 features: 1. 深夜券大量兑换 2.工作群发言异常（突增or突降）3. 购买大容量硬盘

## 以线下数据资产化，赋能实体经济，[曾黎(众盟数据)](http://www.zmeng.cc/)

* 消费升级及新技术，带动新零售快速发展
* 消费者需求变化升级
* 线下消费场景中，有大量痛点没被解决
* 智慧零售趋势——线上线下打通，无缝购物体验
* 数字化是智慧零售方式和方法实现的基础 新零售时代，线下场景成为数据节点

## 大数据实时智能处理技术及应用，[王新根(邦盛科技)](https://www.bsfit.com.cn/)

* 大数据处理： 批式+流式
* 数据的热&冷
* 实时计算发展历程
* AI模型迫切需要落地
* 大数据实时智能处理技术

    * 三核协同智能决策引擎
    * 全栈式知识图谱平台

* 12306, 30B pageview per day, at least 2M TPS required to process.

## 业务增长在分布式事务和大数据融合中的实践，梁福坤(OKCoin)

* OLTP(Online Transaction Processing)

    * 数据量少、面向应用、并行十五处理、分库分表、读写分离、cache技术、

* OLTP(Online Analytical Processing)

    * MDX

* HTAP (Hybrid transactional/analytical processing)

    * 数据实时可见、支持多维度低延迟查询交付、低成本
    * 数据sharding、实例间share nothing、便于横向水平扩展
    * 数据分区
    * 分布式事务

* 存储模型
![](slide_snap/storage_model.jpg "存储模型")

# 演化计算前沿技术论坛 (Oct.27, afternoon)

## Learning Guided Evolutionary Multiobjective Optimization, [周爱民(ECNU)](https://faculty.ecnu.edu.cn/s/1949/t/22631/main.jspy)

![](slide_snap/aimin_conclusion.jpg)

## ZOOpt: 面向机器学习的非梯度优化工具箱 , [俞扬(NJU)](http://lamda.nju.edu.cn/yuy/)

* machine learning = representation + evaluation + optimization
* Derivative-free optimization

    * gradient based: relies on f(x)
    * derivative-free based: use only the function values on f(x)
    * Switch Analysis for Running Time Analysis of Evolutionary Algorithms 
    * Drift analysis and average time complexity of evolutionary algorithms

* Two problem classes

    * selection(pseudo-binary constrained) problems

        * algorithm: Pareto optimization

    * local Holder continuous functions

        * RACOS

* More demands

    * Huge parameters to be optimized 
    * Noisy evaluation
    * Parallel

* Some use cases

    * AutoML
